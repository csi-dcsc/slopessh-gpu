function [centroids] = getError(image1, startGridX, startGridY, endGridX, endGridY, blockSizeX, blockSizeY)
%-------------------------- Get Centroids -------------------------------- 
% This function calculates the centroid of the image points supplied by a
% Shack-Hartmann sensor. The image is divided into a grid with fixed size
% (blockSizeX and blockSizeY). In each block the centroid is calculated by
% locating the Center Of Mass. Note that in each block a dynamic block is
% created to exlude data from neighbouring blocks. The dynamic grid is
% smaller in size than the fixed block. The start position of the dynamic
% grid is calculated by finding the spot with the most information. The 
% start location of the grid (startGridX and startGridY) is found by
% minimizing the error between the reference centroids and the center of 
% each block. Beside the centroids, this function also calculates the slope
% of each block (slopes). The slope is calculated by comparing the centroid
% with the reference centroid. 
%
% The inputs of the function an image and a 3-D matrix with the location of
% the centroid for the reference image (86x87x2).
% The output of the function is a 3-D matrix with the x-y coordinates of
% the centroid for each block in the grid. The function also outputs a
% matrix (slopes) with the calculted slope for both in X and Y direction.
%--------------------------------------------------------------------------
%           [centroids, slopes] = getCentroids(image1, centroidRef)
% Inputs
%   - image         Output of the Shack-Hartmann sensor
%   - centroidRef   3-D matrix with the location of the centroid for the 
%                   reference image
%   - startGridX    The X position in pixels where the grid starts
%   - startGridY    The Y position in pixels where the grid starts
%   - endGridX      The X position in pixels where the grid ends
%   - endGridY      The Y position in pixels where the grid ends
%   - blockSizeX    The size in X direction of one block in the grid
%                   in pixles
%   - blockSizeY    The size in Y direction of one block in the grid
%                   in pixles
% Outputs
%   - centroids     The calculated centroids of the image points of the 
%                   Shack-Hartmann sensor
%             
%--------------------------------------------------------------------------
% E-mail:   M.Griffioen@tudelft.nl
% Date:     11-5-2016 
%-------------------------------------------------------------------------- 

    getBlockCentroid = @(gridXx, gridYy, Imatrix) ...                       % Calculates the location of the center of gravity
                            [ sum(Imatrix *  gridXx') / sum(sum(Imatrix));  % of the data in Imatrix.
                              sum(Imatrix' * gridYy') / sum(sum(Imatrix))]; 
    DEBUG = 0;
       
    image1(image1<10) = 0;
    image1(image1>45) = image1(image1>45)*3;
    if DEBUG
        image(image1)
        hold on
        axis square;
    end
    image1 = double(image1);                                                % Get the double values of the image
    blockEnd = [startGridX+blockSizeX/2 startGridY+blockSizeY/2];           % The position of end of the first block
    gridXVector = startGridX-blockSizeX/2:blockSizeX:endGridX-blockSizeX/2; % Construct the grid in X-direction
    gridYVector = startGridY-blockSizeY/2:blockSizeY:endGridY-blockSizeY/2; % Construct the grid in Y-direction
    
    centroids = nan(size(gridXVector,2), size(gridYVector,2),2,1);          % 3-D matrix for the centroids. Initialized as NAN
 
    for xx = 1:size(gridXVector,2)                                          % loop over the grid in X-direction
        sizeX = gridXVector(xx):blockEnd(1);                           	    % The datavector in x direction for the current block        
        for yy = 1:size(gridYVector,2)                                      % loop over the grid in Y-direction
            centroids(xx,yy,:) = nan;                                       % If no centroid is found, the reference is NAN
            sizeY = gridYVector(yy):blockEnd(2);
            if(yy > 2 && ~isnan(centroids(xx,yy-1,2)))
                sizeY = centroids(xx,yy-1,2) + blockSizeY/2:blockEnd(2);    % use the previous centroid as a base for the next centroid.
            end
            
            blockData = image1(round(sizeY),round(sizeX));                  % Data from the SH in the current block
            if (sum(sum(blockData)) > 0)
                % Dynamic grid
                blockCentroid = getBlockCentroid(sizeX, sizeY, blockData); 
                sizeXD = (sizeX + (blockCentroid(1) - sizeX(1))) - blockSizeX/2; 
                sizeYD = (sizeY + (blockCentroid(2) - sizeY(1))) - blockSizeY/2;
                blockData = image1(round(sizeYD),round(sizeXD));           
                % filter each block
                imageThreshold = 0.35*max(max(blockData));                  
                blockData(blockData<=imageThreshold) = 0;                 
                blockCentroid = getBlockCentroid(sizeXD, sizeYD, blockData);
                if (~isnan(blockCentroid))                                 
                    if DEBUG
                        plot(blockCentroid(1), blockCentroid(2), 'rx')
                        rectangle('position', [sizeXD(1), sizeYD(1), sizeXD(end) - sizeXD(1), sizeYD(end) - sizeYD(1)])
                    end
                    centroids(xx,yy,:) = blockCentroid;                               
                end
            end
      
            blockEnd(2)= blockEnd(2) + blockSizeY;                        
        end        
    
            blockEnd = [blockEnd(1)+blockSizeX, startGridY+blockSizeY/2];     
    end
end
