function [centroids] = getCentroidsWithReference(image1, centroidRef, blockSizeX, blockSizeY)
%-------------------------- Get Centroids -------------------------------- 
% This function calculates the centroid of the image points supplied by a
% Shack-Hartmann sensor. The image is divided into a grid with fixed size
% (blockSizeX and blockSizeY). In each block the centroid is calculated by
% locating the Center Of Mass. Note that in each block a dynamic block is
% created to exlude data from neighbouring blocks. The dynamic grid is
% smaller in size than the fixed block. The start position of the dynamic
% grid is calculated by finding the spot with the most information. The 
% start location of the grid (startGridX and startGridY) is found by
% minimizing the error between the reference centroids and the center of 
% each block. Beside the centroids, this function also calculates the slope
% of each block (slopes). The slope is calculated by comparing the centroid
% with the reference centroid. 
%
% The inputs of the function an image and a 3-D matrix with the location of
% the centroid for the reference image (86x87x2).
% The output of the function is a 3-D matrix with the x-y coordinates of
% the centroid for each block in the grid. The function also outputs a
% matrix (slopes) with the calculted slope for both in X and Y direction.
%--------------------------------------------------------------------------
%           [centroids, slopes] = getCentroids(image1, centroidRef)
% Inputs
%   - image         output of the Shack-Hartmann sensor
%   - centroidRef   3-D matrix with the location of the centroid for the 
%                   reference image (86x87x2)
% Outputs
%   - centroids     The calculated centroids of the image points of the 
%                   Shack-Hartmann sensor (86x87x2)
%   - slopes        Calculated slopes for each centroid in the grid. 
%                   (86x87x2)
%--------------------------------------------------------------------------
% E-mail:   M.Griffioen@tudelft.nl
% Date:     11-5-2016 
%-------------------------------------------------------------------------- 

    getBlockCentroid = @(gridXx, gridYy, Imatrix) ...                       % Calculates the location of the center of gravity
                            [ sum(Imatrix *  gridXx') / sum(sum(Imatrix));  % of the data in Imatrix.
                              sum(Imatrix' * gridYy') / sum(sum(Imatrix))]; 
    image1(image1<10) = 0;
    image1(image1>40) = image1(image1>40)*3;
    image1 = double(image1);
     
    centroids = nan(size(centroidRef,1), size(centroidRef,2),2,1);          
    
    for xx = 1:size(centroidRef,1)                                          % loop over the grid in X-direction
        for yy = 1:size(centroidRef,2)                                      % loop over the grid in Y-direction
            if(~isnan(centroidRef(xx,yy,:)))                                % check if the current block contains a reference centroid 
                centroids(xx,yy,:) = centroidRef(xx,yy,:);                  % If no new centroid is found, the reference is the centroid
                
                tempX = round((centroidRef(xx, yy, 1) - blockSizeX/2):(centroidRef(xx, yy, 1) - blockSizeX/2)+ blockSizeX);
                tempY = round((centroidRef(xx, yy, 2) - blockSizeY/2):(centroidRef(xx, yy, 2) - blockSizeY/2)+ blockSizeY);
                blockData = image1(tempY,tempX);                           
                blockCentroid = getBlockCentroid(tempX, tempY, blockData);  % get the centroid of the dynamic block
                if (~isnan(blockCentroid))                                  % check if a centroid is found
                    centroids(xx,yy,:) = blockCentroid;                     % store the centroid and calculate the slopes (dx/f) && (dy/f)
                end
            end        
        end
    end
end
