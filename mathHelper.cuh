#include <curand.h>
#include <cstdlib>
#include <cublas_v2.h>
#include <iostream>
#include <errno.h>
#include <bmc_mdlib.h>
//#include "cuda.h"

#ifndef _mathHelper  
#define _mathHelper

#define MAXSHAREDMEM 32000
typedef unsigned char unsigned8;
typedef unsigned short unsigned16;

#define allocateMemoryHost(matrix, row, col, z) _allocateMemoryHost(__FILE__, __LINE__, matrix, row, col, z)
#define allocateMemoryDevice(matrix, row, col, z) _allocateMemoryDevice(__FILE__, __LINE__, matrix, row, col, z)
#define copyMatrixToDevice(d_matrix, h_matrix, row, col, z) _copyMatrixToDevice(__FILE__, __LINE__, d_matrix, h_matrix, row, col, z)
#define copyMatrixFromDevice(h_matrix, d_matrix, row, col, z) _copyMatrixFromDevice(__FILE__, __LINE__, h_matrix, d_matrix, row, col, z)

void exitOnFailure(const char *message, const char* file, int number)
{
	printf("\n\n---------------------- FAILURE ----------------------\n\n");
	printf("\t %s(%i): %s\n\n", file, number, message);
	printf("---------------------- EXIT    ----------------------\n\n\n");
	exit (EXIT_FAILURE);
}

void _allocateMemoryHost(const char *filename, int lineno, unsigned char **matrix, int row, int col, int z)
{
	if ((*matrix = (unsigned char *) malloc(row * col * z * sizeof(unsigned char))) == NULL)
	{
		exitOnFailure("Host memory allocation for unsigned chars failed", filename, lineno);
	}
}

void _allocateMemoryHost(const char *filename, int lineno, unsigned short **matrix, int row, int col, int z)
{
	if ((*matrix = (unsigned short *) malloc(row * col * z * sizeof(unsigned short))) == NULL)
	{
		exitOnFailure("Host memory allocation for unsigned chars failed", filename, lineno);
	}
}

void _allocateMemoryHost(const char *filename, int lineno, double **matrix, int row, int col, int z)
{
	if ((*matrix = (double *) malloc(row * col * z * sizeof(double))) == NULL)
	{
		exitOnFailure("Host memory allocation for doubles failed", filename, lineno);
	}
}

void _allocateMemoryHost(const char *filename, int lineno, int **matrix, int row, int col, int z)
{
	if ((*matrix = (int *) malloc(row * col * z * sizeof(int))) == NULL)
	{
		exitOnFailure("Host memory allocation for ints failed", filename, lineno);
	}
}

void _allocateMemoryDevice(const char *filename, int lineno, unsigned char **matrix, int row, int col, int z)
{
	if (cudaMalloc(&*matrix, row * col * z * sizeof(unsigned char)) != cudaSuccess)
	{
		exitOnFailure("Device memory allocation for unsigned chars failed", filename, lineno);
	}
}

void _allocateMemoryDevice(const char *filename, int lineno, unsigned short **matrix, int row, int col, int z)
{
	if (cudaMalloc(&*matrix, row * col * z * sizeof(unsigned short)) != cudaSuccess)
	{
		exitOnFailure("Device memory allocation for unsigned chars failed", filename, lineno);
	}
}

void _allocateMemoryDevice(const char *filename, int lineno, double **matrix, int row, int col, int z)
{
	if (cudaMalloc(&*matrix, row * col * z * sizeof(double)) != cudaSuccess)
	{
		exitOnFailure("Device memory allocation for doubles failed", filename, lineno);
	}
}

void _allocateMemoryDevice(const char *filename, int lineno, int **matrix, int row, int col, int z)
{
	if (cudaMalloc(&*matrix, row * col * z * sizeof(int)) != cudaSuccess)
	{
		exitOnFailure("Device memory allocation for ints failed", filename, lineno);
	}
}

void _copyMatrixToDevice(const char *filename, int lineno, unsigned char *matrix_d, unsigned char *matrix_h, int row, int col, int z)
{
	if (cudaMemcpy(matrix_d, matrix_h, row * col * z * sizeof(unsigned char), cudaMemcpyHostToDevice) != cudaSuccess)
	{
		exitOnFailure("Memory copy host to device for unsigned chars failed", filename, lineno);
	}
}

void _copyMatrixToDevice(const char *filename, int lineno, unsigned short *matrix_d, unsigned short *matrix_h, int row, int col, int z)
{
	if (cudaMemcpy(matrix_d, matrix_h, row * col * z * sizeof(unsigned short), cudaMemcpyHostToDevice) != cudaSuccess)
	{
		exitOnFailure("Memory copy host to device for unsigned chars failed", filename, lineno);
	}
}

void _copyMatrixToDevice(const char *filename, int lineno, double *matrix_d, double *matrix_h, int row, int col, int z)
{
	if (cudaMemcpy(matrix_d, matrix_h, row * col * z * sizeof(double), cudaMemcpyHostToDevice) != cudaSuccess)
	{
		exitOnFailure("Memory copy host to device for doubles failed", filename, lineno);
	}
}

void _copyMatrixToDevice(const char *filename, int lineno, int *matrix_d, int *matrix_h, int row, int col, int z)
{
	if (cudaMemcpy(matrix_d, matrix_h, row * col * z * sizeof(int), cudaMemcpyHostToDevice) != cudaSuccess)
	{
		exitOnFailure("Memory copy host to device for ints failed", filename, lineno);
	}
}

void _copyMatrixFromDevice(const char *filename, int lineno, double *matrix_h, double *matrix_d, int row, int col, int z)
{
	if(cudaMemcpy(matrix_h, matrix_d, row * col * z * sizeof(double), cudaMemcpyDeviceToHost) != cudaSuccess)
	{
		exitOnFailure("Memory copy device to host for doubles failed", filename, lineno);
	}
}

void getDataFromFile(double *matrix, const char *location)
{
	FILE * f;
	f = fopen(location, "r");
	if (f == NULL)
	{
		printf("ERROR! Unable to open/read the file %s. Please check if the file is in the propper format. \n", location);
	}
	else
	{
		int counter = 0;
		double num;
		while (fscanf(f, "%lf", &num) != EOF)
		{
			matrix[counter++] = num;
		}
	}
	fclose(f);
}

void saveMatrix(const double *A, int nr_rows_A, int nr_cols_A, const char *filepath)
{
	FILE *f = fopen(filepath, "w");
	for (int i = 0; i < nr_rows_A; i++)
	{
		for (int j = 0; j < nr_cols_A; j++)
		{
			fprintf(f, "%0.12f ", A[j + i * nr_cols_A]);
		}
		fprintf(f, " \r\n");
	}
}

void printMatrix(const int *A, int nr_rows_A, int nr_cols_A)
{
	for (int i = 0; i < nr_rows_A; i++)
	{
		for (int j = 0; j < nr_cols_A; j++)
		{
			printf("%d ", A[j + i * nr_cols_A]);
		}
		printf(" \r\n");
	}
}

void printMatrix(const double *A, int nr_rows_A, int nr_cols_A)
{
	for (int i = 0; i < nr_rows_A; i++)
	{
		for (int j = 0; j < nr_cols_A; j++)
		{
			printf("%f ", A[j + i * nr_cols_A]);
		}
		printf(" \r\n");
	}
}

void magic(double *A, int nr_rows_A, int nr_cols_A, int centerPoint)
{
	int temp = 1000000;

	for (int i = 0; i < nr_rows_A; i++)
	{
		for (int j = 0; j < nr_cols_A; j++)
		{
			int random_number = rand() % temp + 1 - temp / 2;
			A[j + i * nr_cols_A] = (double) random_number / temp + centerPoint;
		}
	}
}

void magic(tU16 *A, int nr_rows_A, int nr_cols_A, int upperLimit)
{

	for (int i = 0; i < nr_rows_A; i++)
	{
		for (int j = 0; j < nr_cols_A; j++)
		{
			if (rand() % 100 > 80)
				A[j + i * nr_cols_A] = upperLimit;
//			A[j + i * nr_cols_A] = rand() % upperLimit;
		}
	}
}

#endif
