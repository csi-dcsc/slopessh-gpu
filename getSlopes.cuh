/*
 The function getSlopes() calls the CUDA function d_getSlopes() to calculate the slope for each centroid in the Shack-Hartmann image. 
 The location of each centroid is determined and compared to its corresponding reference location.
 
 getSlopes(*d_wavefrontImage, aoi[], *gridSize, blockSize, MAXBLOCKSIZE, *d_refCentroids, *d_slopes)  
 ------------------------------------------------------------------------------------------------------------------------------------
 Inputs
 - *d_wavefrontImage			Pointer to the data of the Shack-Hartmann sensor. This image can be obtained by calling the function: 
 UeyeCapture(int camera_number, unsigned8  *h_wavefrontImage)
 - aoi							The Area Of Interest in the image of the Shack-Hartmann sensor. This variable contains:
 [Xlocation Ylocation Width Height]
 - *gridSize					The gridsize contains the number of reference centroids in X and Y direction. The gridsize is stored 
 in the MATLAB structure data.mat, and can be accessed with the field "C_gridSize". The data.mat file
 is created by running the MATLAB file optimizeGrid.m. This variable contains: 
 [Number_of_centroids_in_X Number_of_centroids_in_Y]
 - blockSize					The size in pixels between two centroids. The blocksize is stored in the MATLAB structure data.mat,
 and can be accessed with the field "C_blockSize". The data.mat file is created by running the MATLAB 
 file optimizeGrid.m.
 - MAXBLOCKSIZE 				The number of the maximum amount of threads in one block. Depends on the graphics card. 
 - *d_refCentroids				Pointer to the reference centroids. The reference centroids are used to locate the initial position 
 of the centroids. The found reference centroids are then used to calculate the slopes. This variable
 is of size: [gridSize(0) gridSize(1)]. The reference centroids are stored in the MATLAB structure 
 data.mat, and can be accessed with the field "C_referenceCentroids". The data.mat file is created by 
 running the MATLAB file optimizeGrid.m.
 - *d_slopes 					Pointer to the block of memory on the GPU where the slopes are stored. The slopes are stored as:
 [Slopes_in_X_direction; Slopes_in_Y_direction]. 
 ------------------------------------------------------------------------------------------------------------------------------------
 */

#include <curand.h>                                                                                                                                                                                                                                               
#include <cstdlib>                                                                                                                                                                                                                                                
#include <cublas_v2.h>                                                                                                                                                                                                                                            
#include <iostream>                                                                                                                                                                                                                                               
#include "cuda.h"                                                                                                                                                                                                                                                 

#ifndef _getSlopes                                                                                                                                                                                        
#define _getSlopes                                                                                                                                                                                                                                                

__global__
void d_getSlopes(unsigned short *d_wavefrontImage, double blockSize, int wfWidth, int centroidsNrX, int centroidsNrY, double *d_refCentroids, double *d_slopes) //TODO: make a struct with image info                                                                                            
{
	int x, y, width, threadID, allCentroids;
	double centroidX = 0, centroidY = 0;
	x = threadIdx.x + blockIdx.x * blockDim.x;
	y = threadIdx.y + blockIdx.y * blockDim.y;
	width = centroidsNrX;
	allCentroids = centroidsNrX * centroidsNrY;
	threadID = x + y * width;

	if (x < centroidsNrX && y < centroidsNrY)
	{
		if (!(isnan(d_refCentroids[threadID]) || isnan(d_refCentroids[threadID + allCentroids]) || d_refCentroids[threadID] == 0 || d_refCentroids[threadID + allCentroids] == 0))
		{
			int rowSummed = 0;
			int columnSummed = 0;
			int totalSum = 0;
			int rowSummedWeighted = 0;
			int columnSummedWeighted = 0;
			double currentX, currentY;
			int currentIndex;
			int noiseThreshold = 0;
			double blockSizeHalf = blockSize / 2;

			currentX = round(d_refCentroids[threadID] - blockSizeHalf);
			currentY = round(d_refCentroids[threadID + allCentroids] - blockSizeHalf);
			currentIndex = (int) (currentX + currentY * wfWidth);

			for (int xx = 0; xx < blockSize; xx++)
			{
				rowSummed = 0;
				columnSummed = 0;
				for (int yy = 0; yy < blockSize; yy++)
				{
					if (d_wavefrontImage[currentIndex + yy + xx * wfWidth] >= noiseThreshold)
					{
							rowSummed += d_wavefrontImage[currentIndex + yy + xx * wfWidth];
					}
					if (d_wavefrontImage[currentIndex + xx + yy * wfWidth] >= noiseThreshold)
					{
							columnSummed += d_wavefrontImage[currentIndex + xx + yy * wfWidth];
					}
				}
				totalSum += rowSummed;
				rowSummedWeighted += rowSummed * xx;
				columnSummedWeighted += columnSummed * xx;
			}
			centroidX = (currentX + (columnSummedWeighted / (double) totalSum));
			centroidY = (currentY + (rowSummedWeighted / (double) totalSum));
			__syncthreads();

			if (!isnan(centroidX) && !isnan(centroidY))
			{
				d_slopes[threadID] = d_refCentroids[threadID] - centroidX;
				d_slopes[threadID + allCentroids] = d_refCentroids[threadID + allCentroids] - centroidY;
			} else
			{
				d_slopes[threadID] = 0;
				d_slopes[threadID + allCentroids] = 0;
			}
		} else
		{
			d_slopes[threadID] = 0;
			d_slopes[threadID + allCentroids] = 0;

		}
	}

}

void getSlopes(unsigned short *d_wavefrontImage, int aoi[], double *gridSize, double blockSize, int MAXBLOCKSIZE, double *d_refCentroids, double *d_slopes)
{
	int threadsPerBlock = (gridSize[0] > MAXBLOCKSIZE ? MAXBLOCKSIZE : gridSize[0]);
	dim3 blockDim(threadsPerBlock, threadsPerBlock);
	dim3 grid(ceil((float) gridSize[0] / threadsPerBlock), ceil((float) gridSize[1] / threadsPerBlock));
d_getSlopes<<<grid, blockDim>>>(d_wavefrontImage, blockSize, aoi[2], gridSize[0], gridSize[1], d_refCentroids, d_slopes);
}

#endif                                                                                                                                                                                                                                                            
