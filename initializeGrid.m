%% initialize matlab
clear all; close all; clc; format compact;

%% load the Shack-Hartmann image
[FileName,PathName] = uigetfile('*.mat','Select the Shack-Hartmann image');
try
    data = load([PathName FileName]);
    img = data.img';                                                     
catch
    error('File not found.')
end

%% rotate the SH
rc = 0.696639422814938;
imgCorrected = imrotate(img, rc, 'crop');

%% Determine the grid
figure('units','normalized','outerposition',[0 0 1 1])
image(imgCorrected); hold on;
axis square
clickString = {'Left Column of Centroids' 'Right Column of Centroids' 'Top Row of Centroids' 'Bottom Row of Centroids'} ;
for ii = 1:4
    title(['ZOOM IN AND SELECT THE ' clickString(ii) '. PRESS KEY AND CLICK ON CENTER OF A CENTROID'])
    zoom on;
    pause()
    [x,y] = ginput(1);
   switch ii
        case 1
            startGridX = x;
            plot([x x], [0 size(img,2)], ':r', 'LineWidth', 2)
        case 2
            endGridX = x;
            plot([x x], [0 size(img,2)], ':r', 'LineWidth', 2)                
       case 3
            startGridY = y;
            plot([0 size(img,1)], [y y], ':r', 'LineWidth', 2)
            tempSize = endGridX - startGridX;
            plot([0 size(img,1)], [y+tempSize y+tempSize], ':g', 'LineWidth', 2)
       case 4
            endGridY = y;
            plot([0 size(img,1)], [y y], ':r', 'LineWidth', 2)
   end
    zoom out
end 
close all;
pause(1)

%% calculate blocksize
Fs =  1;                                                                   % fs*t=N. N=2048, t=2048
N = size(imgCorrected,1);
FX = sum(imgCorrected,1);        FY = sum(imgCorrected',2);    
Fx = fftshift(fft(FX)); Fy = fftshift(fft(FY));
dF = Fs/N;
f = -Fs/2:dF:Fs/2-dF; 
Fx = abs(Fx)/N;
Fy = abs(Fy)/N;
factor = 0.2;	% this value needs to be changed according to the application
peakHeight = round((max(Fx) + max(Fy)) * factor);
try
    [~, Fxloc] = findpeaks(Fx,'NPEAKS',10, 'MINPEAKHEIGHT',peakHeight);
    [~, Fyloc] = findpeaks(Fy,'NPEAKS',10, 'MINPEAKHEIGHT',peakHeight);
    blockSizeX = 1 / f(Fxloc(end));
    blockSizeY = 1 / f(Fyloc(end));
    if(floor(blockSizeX) ~= 11)
        disp('WARNING! - The expected blocksize should be around 11.4. Check if the found peak is correct.')
        figure;
        plot(Fx);
        hold on
        plot(Fxloc, Fx(Fxloc), 'rx');
    end

catch    
    figure;    
    plot(Fx);
    title(['PeakHeight threshold = ' num2str(peakHeight)]);
    hold on
    plot(Fxloc, Fx(Fxloc), 'rx');
    error('error. The found peak is not correct...')
end

%% Get Centroids
[centroids] = getCentroids(imgCorrected, startGridX, startGridY, endGridX, endGridY, blockSizeX, blockSizeY);
% rotate the centroids to compensate for the error in the sensor
rc1 = deg2rad(rc);
rot = [cos(rc1) -sin(rc1); sin(rc1) cos(rc1)];
cRot = centroids;
for i = 1:size(centroids,1)
    for j=1:size(centroids,2)
        cRot(i,j,:) = rot*[centroids(i,j,1)-1024; centroids(i,j,2)-1024] + 1024;
    end
end
% improve on result with found centroid locations:
improvedCentroids1 = getCentroidsWithReference(img, cRot, blockSizeX, blockSizeY);
improvedCentroids = getCentroidsWithReference(img, improvedCentroids1, blockSizeX, blockSizeY);

%% Create DATA structure
C_gridSize = [size(improvedCentroids,1) size(improvedCentroids,2)];
C_blockSize = blockSizeX;
C_referenceCentroids = [improvedCentroids(:,:,1) - 1; improvedCentroids(:,:,2) - 1]'; 
C_created = ['Initialization data created on: ' datestr(datetime('now')) '. Gridsize = [' num2str(size(improvedCentroids,1)) ', ' num2str(size(improvedCentroids,2)), '].'];
C_nrOfActuators = 952;

save('data', 'C_blockSize', 'C_gridSize', 'C_referenceCentroids', 'C_created');
disp('data.mat structure created. ');
%
image(img)
axis square;
xlabel('pixels')
ylabel('pixels')
hold on
% plot(cRot(:,:,1), cRot(:,:,2), 'rx')
plot(improvedCentroids(:,:,1), improvedCentroids(:,:,2), 'rx')
legend('found reference centroids')

